<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220825171429 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE data_bc (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, sexe enum(\'homme\', \'femme\'), height NUMERIC(3, 2) NOT NULL, weight INT NOT NULL, calculate_date DATE NOT NULL, activity enum(\'inactif\', \'légèrement actif\', \'actif\', \'très actif\'), bc VARCHAR(255) NOT NULL, INDEX IDX_4A65ED54A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_imc (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, height NUMERIC(3, 2) NOT NULL, weight INT NOT NULL, calculate_date DATE NOT NULL, imc VARCHAR(255) NOT NULL, INDEX IDX_F823ED00A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data_bc ADD CONSTRAINT FK_4A65ED54A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE data_imc ADD CONSTRAINT FK_F823ED00A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE data_bc DROP FOREIGN KEY FK_4A65ED54A76ED395');
        $this->addSql('ALTER TABLE data_imc DROP FOREIGN KEY FK_F823ED00A76ED395');
        $this->addSql('DROP TABLE data_bc');
        $this->addSql('DROP TABLE data_imc');
    }
}
