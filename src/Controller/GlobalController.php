<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Entity\DataImc;
use App\Form\DataImcType;
use App\Repository\DataImcRepository;
use App\Entity\DataBc;
use App\Form\DataBcType;
use App\Repository\DataBcRepository;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Security\LoginAuthenticator;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GlobalController extends AbstractController
{
    /**
     * @Route("/profile", name="profile" , methods={"GET", "POST"})
     */
    public function index(Request $request, UserRepository $userRepository, DataImcRepository $dataImcRepository, DataBcRepository $dataBcRepository): Response
    {
        if ($this->isGranted('ROLE_USER')) {

            $user = $this->getUser();
        
            $form = $this->createForm(UserType::class, $user);
            $form->handleRequest($request);
        
            if ($form->isSubmitted() && $form->isValid()) {
                $userRepository->add($user, true);

                return $this->redirectToRoute('profile', [], Response::HTTP_SEE_OTHER);
            }

            $dataImc = new DataImc();
            $dataImc->setCalculateDate(new \DateTime('now'));
            $dataImc->setUser($user);
    
            $formImc = $this->createForm(DataImcType::class, $dataImc);
            $formImc->handleRequest($request);

            $dataBc = new DataBc();
            $dataBc->setCalculateDate(new \DateTime('now'));
            $dataBc->setUser($user);

            $formBc = $this->createForm(DataBcType::class, $dataBc);
            $formBc->handleRequest($request);

            $ctaString = "Besoin d'un accompagnement diététique ?";


            if ($formImc->isSubmitted() && $formImc->isValid()) {

                $imcResult = ($dataImc->getWeight()) / ($dataImc->getHeight()*$dataImc->getHeight());
                $dataImc->setImc(round($imcResult,2));

                if ($imcResult < 18.5) {

                    $ctaString = "Vous êtes maigre ? Nous proposons des programmes diététique adapté a vos besoin.";
                }

                if ($imcResult > 18.4 && $imcResult < 25) {

                    $ctaString = "Vous êtes normale ? Mais vous souhaitez faire du sport ? Contactez un de nos Coach sportif.";
                }

                if ($imcResult > 24.9 && $imcResult < 30) {

                    $ctaString = "Vous êtes en surpoids ? Nous vous proposons un accompagnement diététique et sportif adapté.";
                }

                if ($imcResult > 29.9 && $imcResult < 35) {

                    $ctaString = "Vous êtes en obésité modérée ? Nous vous proposons un accompagnement diététique et sportif adapté.";
                }

                if ($imcResult > 29.9 && $imcResult < 40) {

                    $ctaString = "Vous êtes en obésité sévère ? Contactez un de nos expert médicale.";
                }

                if ($imcResult > 39.9 ) {

                    $ctaString = "Vous êtes en obésité morbide ? Contactez un de nos expert médicale.";
                }
            
                $dataImcRepository->add($dataImc, true);

                // return $this->redirectToRoute('profile', [], Response::HTTP_SEE_OTHER);
                return $this->render('global/index.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                    'data_imc' => $dataImc,
                    'formImc' => $formImc->createView(),
                    'imcList' => $dataImcRepository->userList($user->id),
                    'data_bc' => $dataBc,
                    'formBc' => $formBc->createView(),
                    'bcList' => $dataBcRepository->userList($user->id),
                    'ctaString' => $ctaString,
                ]);
    
            }

    
            if ($formBc->isSubmitted() && $formBc->isValid()) {


                if($dataBc->getSexe() == "homme") {
                    $bcResult = (13.707*$dataBc->getWeight()) + (492.3*$dataBc->getHeight()) - (6.673*$user->age);

                    if ($dataBc->activity == "inactif") {
                        $bcResult = $bcResult*1.2;
                    }
    
                    if ($dataBc->activity == "légèrement actif") {
                        $bcResult = $bcResult*1.375;
                    }
    
                    if ($dataBc->activity == "actif") {
                        $bcResult = $bcResult*1.55;
                    }
    
                    if ($dataBc->activity == "très actif") {
                        $bcResult = $bcResult*1.725;
                    }

                    $dataBc->setBc(round($bcResult,2));

                }

                if($dataBc->getSexe() == "femme") {
                    $bcResult = (9.740*$dataBc->getWeight()) + (172.9*$dataBc->getHeight()) - (4.737*$user->age);

                    if ($dataBc->activity == "inactif") {
                        $bcResult = $bcResult*1.2;
                    }
    
                    if ($dataBc->activity == "légèrement actif") {
                        $bcResult = $bcResult*1.375;
                    }
    
                    if ($dataBc->activity == "actif") {
                        $bcResult = $bcResult*1.55;
                    }
    
                    if ($dataBc->activity == "très actif") {
                        $bcResult = $bcResult*1.725;
                    }

                    $dataBc->setBc(round($bcResult,2));

                }

           
                $dataBcRepository->add($dataBc, true);
                // return $this->redirectToRoute('profile', [], Response::HTTP_SEE_OTHER);
                return $this->render('global/index.html.twig', [
                    'user' => $user,
                    'form' => $form->createView(),
                    'data_imc' => $dataImc,
                    'formImc' => $formImc->createView(),
                    'imcList' => $dataImcRepository->userList($user->id),
                    'data_bc' => $dataBc,
                    'formBc' => $formBc->createView(),
                    'bcList' => $dataBcRepository->userList($user->id),
                    'ctaString' => $ctaString,
            
                ]);
            }


            return $this->render('global/index.html.twig', [
                'user' => $user,
                'form' => $form->createView(),
                'data_imc' => $dataImc,
                'formImc' => $formImc->createView(),
                'imcList' => $dataImcRepository->userList($user->id),
                'data_bc' => $dataBc,
                'formBc' => $formBc->createView(),
                'bcList' => $dataBcRepository->userList($user->id),
                'ctaString' => $ctaString,
            ]);
        }

        else {

            return $this->redirectToRoute('loghome');
        }
    }

      /**
     * @Route("/new", name="newAccount", methods={"GET", "POST"})
     */
    public function new(Request $request,UserPasswordHasherInterface $userPasswordHasher, UserAuthenticatorInterface $userAuthenticator, LoginAuthenticator $authenticator, UserRepository $userRepository): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );

            $userRepository->add($user, true);

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );

        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
    
     /**
     * @Route("/{id}", name="deleteAccount", methods={"POST"})
     */
    public function delete(Request $request, User $user, UserRepository $userRepository,DataImcRepository $dataImcRepository, DataBcRepository $dataBcRepository): Response
    {
        // $user = $this->getUser();
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            foreach($user->getDataBcs() as $e ) {
                $dataBcRepository->remove($e, true);
            }
            foreach($user->getDataImcs() as $e ) {
                $dataImcRepository->remove($e, true);
            }
            $userRepository->remove($user, true);

            return $this->redirectToRoute('loghome', [], Response::HTTP_SEE_OTHER);
        }

        return $this->redirectToRoute('loghome', [], Response::HTTP_SEE_OTHER);
    }

}
