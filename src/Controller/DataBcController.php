<?php

namespace App\Controller;

use App\Entity\DataBc;
use App\Form\DataBcType;
use App\Repository\DataBcRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/data/bc")
 */
class DataBcController extends AbstractController
{
    /**
     * @Route("/", name="app_data_bc_index", methods={"GET"})
     */
    public function index(DataBcRepository $dataBcRepository): Response
    {
        return $this->render('data_bc/index.html.twig', [
            'data_bcs' => $dataBcRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_data_bc_new", methods={"GET", "POST"})
     */
    public function new(Request $request, DataBcRepository $dataBcRepository): Response
    {
        $dataBc = new DataBc();
        $form = $this->createForm(DataBcType::class, $dataBc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataBcRepository->add($dataBc, true);

            return $this->redirectToRoute('app_data_bc_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('data_bc/new.html.twig', [
            'data_bc' => $dataBc,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_data_bc_show", methods={"GET"})
     */
    public function show(DataBc $dataBc): Response
    {
        return $this->render('data_bc/show.html.twig', [
            'data_bc' => $dataBc,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_data_bc_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, DataBc $dataBc, DataBcRepository $dataBcRepository): Response
    {
        $form = $this->createForm(DataBcType::class, $dataBc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataBcRepository->add($dataBc, true);

            return $this->redirectToRoute('app_data_bc_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('data_bc/edit.html.twig', [
            'data_bc' => $dataBc,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_data_bc_delete", methods={"POST"})
     */
    public function delete(Request $request, DataBc $dataBc, DataBcRepository $dataBcRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$dataBc->getId(), $request->request->get('_token'))) {
            $dataBcRepository->remove($dataBc, true);
        }

        return $this->redirectToRoute('app_data_bc_index', [], Response::HTTP_SEE_OTHER);
    }
}
