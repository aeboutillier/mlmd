<?php

namespace App\Controller;

use App\Entity\DataImc;
use App\Form\DataImcType;
use App\Repository\DataImcRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/data/imc")
 */
class DataImcController extends AbstractController
{
    /**
     * @Route("/", name="app_data_imc_index", methods={"GET"})
     */
    public function index(DataImcRepository $dataImcRepository): Response
    {
        return $this->render('data_imc/index.html.twig', [
            'data_imcs' => $dataImcRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_data_imc_new", methods={"GET", "POST"})
     */
    public function new(Request $request, DataImcRepository $dataImcRepository): Response
    {
        $dataImc = new DataImc();
        $form = $this->createForm(DataImcType::class, $dataImc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataImcRepository->add($dataImc, true);

            return $this->redirectToRoute('app_data_imc_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('data_imc/new.html.twig', [
            'data_imc' => $dataImc,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_data_imc_show", methods={"GET"})
     */
    public function show(DataImc $dataImc): Response
    {
        return $this->render('data_imc/show.html.twig', [
            'data_imc' => $dataImc,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_data_imc_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, DataImc $dataImc, DataImcRepository $dataImcRepository): Response
    {
        $form = $this->createForm(DataImcType::class, $dataImc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataImcRepository->add($dataImc, true);

            return $this->redirectToRoute('app_data_imc_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('data_imc/edit.html.twig', [
            'data_imc' => $dataImc,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_data_imc_delete", methods={"POST"})
     */
    public function delete(Request $request, DataImc $dataImc, DataImcRepository $dataImcRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$dataImc->getId(), $request->request->get('_token'))) {
            $dataImcRepository->remove($dataImc, true);
        }

        return $this->redirectToRoute('app_data_imc_index', [], Response::HTTP_SEE_OTHER);
    }
}
