<?php

namespace App\Form;

use App\Entity\DataBc;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DataBcType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('sexe', ChoiceType::class,['choices' => ['Homme'=>'homme','Femme'=>"femme"]])
            ->add('height')
            ->add('weight')
            // ->add('calculate_date')
            ->add('activity', ChoiceType::class,['choices' => [
                'Inactif'=>"inactif",
                'Légèrement Actif'=>"légèrement actif",
                'Actif'=>"actif",
                'Très Actif'=>"très actif"]])
            // ->add('bc')
            // ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DataBc::class,
        ]);
    }
}
