<?php

namespace App\Repository;

use App\Entity\DataImc;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DataImc>
 *
 * @method DataImc|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataImc|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataImc[]    findAll()
 * @method DataImc[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataImcRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataImc::class);
    }

    public function userList($id)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user = (:id)')
            ->setParameter(':id', $id)
            ->orderBy('t.calculate_date', 'DESC')
            ->setMaxResults(3)

            ->getQuery()
            ->getResult()
            ;
    }

   

    public function add(DataImc $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(DataImc $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return DataImc[] Returns an array of DataImc objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?DataImc
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
