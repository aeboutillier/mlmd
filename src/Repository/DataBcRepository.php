<?php

namespace App\Repository;

use App\Entity\DataBc;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DataBc>
 *
 * @method DataBc|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataBc|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataBc[]    findAll()
 * @method DataBc[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataBcRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataBc::class);
    }


    public function userList($id)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user = (:id)')
            ->setParameter(':id', $id)
            ->orderBy('t.calculate_date', 'DESC')
            ->setMaxResults(3)

            ->getQuery()
            ->getResult()
            ;
    }

    public function add(DataBc $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(DataBc $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return DataBc[] Returns an array of DataBc objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?DataBc
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
