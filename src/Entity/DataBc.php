<?php

namespace App\Entity;

use App\Repository\DataBcRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DataBcRepository::class)
 */
class DataBc
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", columnDefinition="enum('homme', 'femme')")
     */
    private $sexe;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=2)
     */
    private $height;

    /**
     * @ORM\Column(type="integer")
     */
    private $weight;

    /**
     * @ORM\Column(type="date")
     */
    public $calculate_date;

    /**
     * @ORM\Column(type="string", columnDefinition="enum('inactif', 'légèrement actif', 'actif', 'très actif')")
     */
    public $activity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bc;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="dataBcs")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(float $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getCalculateDate(): ?\DateTimeInterface
    {
        return $this->calculate_date;
    }

    public function setCalculateDate(\DateTimeInterface $calculate_date): self
    {
        $this->calculate_date = $calculate_date;

        return $this;
    }

    public function getActivity(): ?string
    {
        return $this->activity;
    }

    public function setActivity(string $activity): self
    {
        $this->activity = $activity;

        return $this;
    }

    public function getBc(): ?string
    {
        return $this->bc;
    }

    public function setBc(string $bc): self
    {
        $this->bc = $bc;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
