<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="integer")
     */
    public $age;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\OneToMany(targetEntity=DataImc::class, mappedBy="user")
     */
    private $dataImcs;

    /**
     * @ORM\OneToMany(targetEntity=DataBc::class, mappedBy="user")
     */
    private $dataBcs;

   

    public function __construct()
    {
        $this->dataImcs = new ArrayCollection();
        $this->dataBcs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection<int, DataImc>
     */
    public function getDataImcs(): Collection
    {
        return $this->dataImcs;
    }

    public function addDataImc(DataImc $dataImc): self
    {
        if (!$this->dataImcs->contains($dataImc)) {
            $this->dataImcs[] = $dataImc;
            $dataImc->setUser($this);
        }

        return $this;
    }

    public function removeDataImc(DataImc $dataImc): self
    {
        if ($this->dataImcs->removeElement($dataImc)) {
            // set the owning side to null (unless already changed)
            if ($dataImc->getUser() === $this) {
                $dataImc->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, DataBc>
     */
    public function getDataBcs(): Collection
    {
        return $this->dataBcs;
    }

    public function addDataBc(DataBc $dataBc): self
    {
        if (!$this->dataBcs->contains($dataBc)) {
            $this->dataBcs[] = $dataBc;
            $dataBc->setUser($this);
        }

        return $this;
    }

    public function removeDataBc(DataBc $dataBc): self
    {
        if ($this->dataBcs->removeElement($dataBc)) {
            // set the owning side to null (unless already changed)
            if ($dataBc->getUser() === $this) {
                $dataBc->setUser(null);
            }
        }

        return $this;
    }


}
